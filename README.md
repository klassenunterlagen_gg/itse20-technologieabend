# ITSE21 - Technologieabend Teil NoSQL



![tbz_logo](./x_gitresources/tbz_logo.png)




## Grundkenntnisse

* [Modul 165 BiVO 2021](https://gitlab.com/klassenunterlagen_gg/m165_ap21b)
* [db-engines](https://db-engines.com/de/ranking)
* [JSON und XML](https://gitlab.com/klassenunterlagen_gg/units/json_xml)
* [open data](https://gitlab.com/klassenunterlagen_gg/units/opendata)


## Unterscheidungsmerkmale

* [Warum NoSQL?](https://neo4j.com/blog/why-nosql-databases/)
* [SQL vs. NoSQL](https://www.talend.com/de/resources/sql-vs-nosql/)
* [Skalierbarkeit](https://www.datacenter-insider.de/was-ist-skalierbarkeit-a-852037/)
* Sharding
  * [Sharding - simpel](https://db-engines.com/de/article/Sharding)
  * [Sharding - Bitcoin](https://www.bitcoinsuisse.com/de/news/was-ist-sharding)
  * [Sharding - vertieft](https://www.bitpanda.com/academy/de/lektionen/was-bedeutet-sharding/)
* ACID versus BASE
  * [BASE - simpel](https://db-engines.com/de/article/BASE)
  * [BASE - vertieft](https://wikis.gm.fh-koeln.de/Datenbanken/BASE) - mit Link auf das [CAP-Theorem](https://wikis.gm.fh-koeln.de/Datenbanken/CAP)

## Systeme

* [Neo4J](https://neo4j.com/docs/getting-started/current/)
* [MongoDB - System](https://www.mongodb.com/)
* [MongoDB - University](https://learn.mongodb.com/)
* [InfluxDB](https://www.influxdata.com/products/influxdb-overview/)
* [InfluxDB lernen](https://docs.influxdata.com/influxdb/v2.5/)
* [Vertica](https://www.vertica.com/)
  * [Michael Stonebreaker](https://de.wikipedia.org/wiki/Michael_Stonebraker)
  * [Stonebreaker und big data - Youtube](https://www.youtube.com/watch?v=KRcecxdGxvQ)
* [Cassandra - wide column](https://cassandra.apache.org/_/index.html)
* [Cassandra Web-Buch](./x_gitresources/CassandraTDG_revised3e.pdf)

## erweiterte Themen

* [Applikationssicherheit](https://www.tbzwiki.ch/index.php?title=Modul_183)
* [e-Business und SEO](https://www.tbzwiki.ch/index.php?title=Modul_150)
  * [Unterlagen](https://tbzedu.sharepoint.com/:f:/s/HF_ITSE20_DB/Etd4U-Iywg5OgjU2jvq2-MoBRWNT1PoNwBsb6UZVal654w?e=sIMHF0)


------
